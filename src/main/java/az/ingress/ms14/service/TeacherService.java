package az.ingress.ms14.service;

import az.ingress.ms14.controller.Teacher;
import az.ingress.ms14.repository.TeacherRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {

    private final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public void update(Teacher teacher) {
        teacherRepository.save(teacher);
    }

//    public List<Teacher> updateAll(List<Teacher> teacherList) {
//         return teacherRepository.saveAll(teacherList);
//    }


    public void create(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    public Teacher get(Integer id) {
        return teacherRepository.findById(id).get();
    }
//
//    public List<Teacher> getAll() {
//        return teacherRepository.findAll();
//    }

    public void delete(Integer id) {
        teacherRepository.deleteById(id);
    }
//
//    public void deleteAll() {
//        teacherRepository.deleteAll();
//    }
}
